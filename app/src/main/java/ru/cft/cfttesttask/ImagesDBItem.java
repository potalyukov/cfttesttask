package ru.cft.cfttesttask;

public class ImagesDBItem {
    private long id;
    private String filename;

    public ImagesDBItem(String filename) {
        this.filename = filename;
    }

    public ImagesDBItem(String filename, int id) {
        this.filename = filename;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
