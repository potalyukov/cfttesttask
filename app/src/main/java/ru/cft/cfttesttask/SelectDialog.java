package ru.cft.cfttesttask;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.Random;

public class SelectDialog extends DialogFragment implements View.OnClickListener {

    public static final String[] TEST_STRINGS = {
            "http://cs7011.vk.me/v7011858/165fb/ejDXJ2RsZkY.jpg",
            "http://cs7011.vk.me/v7011858/15f2b/kf5LzD5X_zs.jpg",
            "http://cs7011.vk.me/v7011858/15fd8/FZ3iHsyjkBA.jpg",
            "http://cs7011.vk.me/v7011858/16111/SXTM5p6ux8Q.jpg",
            "http://cs7011.vk.me/v7011858/16161/GM3o35HACPo.jpg",
            "http://cs7011.vk.me/v7011858/161fb/apnIiymR3xM.jpg",
            "http://cs7011.vk.me/v7011858/1626c/O_QZRELHyCY.jpg",
            "http://cs7011.vk.me/v7011858/162b3/iri8wzvTXz8.jpg",
            "http://cs7011.vk.me/v7011858/162a1/UQFGX-BEaiQ.jpg",
            "http://cs7011.vk.me/v7011858/163b5/D9v4kts57GM.jpg",
            "http://cs7011.vk.me/v7011858/16474/f587r4yB8ao.jpg"
    };

    Random random = new Random(System.currentTimeMillis());

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.select));
        View v = inflater.inflate(R.layout.select_image_dialog, null);
        v.findViewById(R.id.dialog_btn_load).setOnClickListener(this);
        v.findViewById(R.id.dialog_btn_camera).setOnClickListener(this);
        v.findViewById(R.id.dialog_btn_gallery).setOnClickListener(this);
        return v;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_btn_load:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getString(R.string.enter_url));
                final EditText input = new EditText(getActivity());

                //input.setText(TEST_STRINGS[random.nextInt(TEST_STRINGS.length)]);

                input.setText(R.string.http);
                int pos = getString(R.string.http).length();
                input.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
                builder.setView(input);
                input.setSelection(pos);
                final MainActivity mainActivity = (MainActivity) getActivity();

                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mainActivity.loadImage(input.getText().toString());
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case R.id.dialog_btn_camera:
                ((MainActivity) getActivity()).dispatchTakePictureIntent();
                break;
            case R.id.dialog_btn_gallery:
                ((MainActivity) getActivity()).pickImage();
                break;
        }
        dismiss();
    }

}