package ru.cft.cfttesttask;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;

import java.util.Random;


public class FilterService extends IntentService {
    public static final String ACTION_ROTATE = "ru.cft.cfttesttask.action.ROTATE";
    public static final String ACTION_MIRROR = "ru.cft.cfttesttask.action.MIRROR";
    public static final String ACTION_INVERT = "ru.cft.cfttesttask.action.INVERT";
    public static final String ACTION_CHECK_SERVICE_STATE = "ru.cft.cfttesttask.action.CHECK";

    public static final String ACTION_RESULT_DONE = "ru.cft.cfttesttask.result.DONE";
    public static final String ACTION_RESULT_PROGRESS = "ru.cft.cfttesttask.result.PROGRESS";
    public static final String ACTION_RESULT_SERVICE_STATE = "ru.cft.cfttesttask.result.SERVICE_STATE";

    public static final String EXTRA_FILENAME = "ru.cft.cfttesttask.extra.FILENAME";
    public static final String EXTRA_ID = "ru.cft.cfttesttask.extra.ID";
    public static final String EXTRA_PROGRESS = "ru.cft.cfttesttask.extra.PROGRESS";
    public static final String EXTRA_IS_RUNNING = "ru.cft.cfttesttask.extra.IS_RUNNING";

    public static final String LOG_TAG = FilterService.class.getCanonicalName();
    public static final Random random = new Random(System.currentTimeMillis());

    private static volatile int nWorkingThreads = 0;

    private ImagesDatabaseHelper dbHelper;

    public static void startActionCheckServiceState(Context context) {
        Log.d(LOG_TAG, "startActionCheckServiceState");
        Intent intent = new Intent(context, FilterService.class);
        intent.setAction(ACTION_CHECK_SERVICE_STATE);
        context.startService(intent);
    }

    public static void startActionRotate(Context context, String filename, int id) {
        Log.d(LOG_TAG, "startActionRotate");
        Intent intent = new Intent(context, FilterService.class);
        intent.setAction(ACTION_ROTATE);
        intent.putExtra(EXTRA_FILENAME, filename);
        intent.putExtra(EXTRA_ID, id);
        context.startService(intent);
    }

    public static void startActionMirror(Context context, String filename, int id) {
        Log.d(LOG_TAG, "startActionMirror");
        Intent intent = new Intent(context, FilterService.class);
        intent.setAction(ACTION_MIRROR);
        intent.putExtra(EXTRA_FILENAME, filename);
        intent.putExtra(EXTRA_ID, id);
        context.startService(intent);
    }

    public static void startActionInvert(Context context, String filename, int id) {
        Log.d(LOG_TAG, "startActionInvert");
        Intent intent = new Intent(context, FilterService.class);
        intent.setAction(ACTION_INVERT);
        intent.putExtra(EXTRA_FILENAME, filename);
        intent.putExtra(EXTRA_ID, id);
        context.startService(intent);
    }


    public FilterService() {
        super("FilterService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (dbHelper == null) {
            dbHelper = new ImagesDatabaseHelper(this);
        }
        Log.d(LOG_TAG, "onHandleIntent");
        if (intent != null) {
            final String action = intent.getAction();
            if (action == ACTION_CHECK_SERVICE_STATE) {
                Intent resultIntent = new Intent(ACTION_RESULT_SERVICE_STATE);
                resultIntent.putExtra(EXTRA_IS_RUNNING, nWorkingThreads != 0);
                sendBroadcast(resultIntent);
                return;
            }
            final int id = intent.getIntExtra(EXTRA_ID, 0);
            final String filename = intent.getStringExtra(EXTRA_FILENAME);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    nWorkingThreads++;
                    try {
                        switch (action) {
                            case ACTION_ROTATE:
                                handleActionRotate(filename, id);
                                break;
                            case ACTION_MIRROR:
                                handleActionMirror(filename, id);
                                break;
                            case ACTION_INVERT:
                                handleActionInvert(filename, id);
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        dbHelper.deleteImagesDBItem(new ImagesDBItem(filename, id));
                    }
                    nWorkingThreads--;
                }
            }
            ).start();

        }
    }

    private void makeResult(Bitmap bitmap, int id) {
        Log.d(LOG_TAG, "makeResult: " + id);
        Intent intent = new Intent(ACTION_RESULT_DONE);
        String filename = null;
        try {
            filename = getExternalFilesDir(null).getAbsolutePath() + "/" + System.currentTimeMillis() + ".png";
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        ImagesDBItem item = new ImagesDBItem(filename);
        item.setId(id);
        try {
            intent.putExtra(EXTRA_FILENAME, filename);
            intent.putExtra(EXTRA_ID, id);
            if (Utils.saveBitmapToFile(bitmap, filename)) {
                dbHelper.updateImagesDBItem(item);
                sendBroadcast(intent);
            } else {
                MainActivity.makeErrorToast(getApplicationContext());
                dbHelper.deleteImagesDBItem(item);
            }
        } catch (Exception e) {
            MainActivity.makeErrorToast(getApplicationContext());
            dbHelper.deleteImagesDBItem(item);
            e.printStackTrace();
        }
    }

    private void sendProgress(int id, int progress) {
        Intent intent = new Intent(ACTION_RESULT_PROGRESS);
        intent.putExtra(EXTRA_ID, id);
        intent.putExtra(EXTRA_PROGRESS, progress);
        sendBroadcast(intent);

    }

    private void handleActionRotate(String filename, int id) {
        Log.d(LOG_TAG, "handleActionRotate");
        emulateLongWork(id);
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap sourceBitmap = BitmapFactory.decodeFile(filename);
        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);

        makeResult(resultBitmap, id);
    }

    private void handleActionMirror(String filename, int id) {
        Log.d(LOG_TAG, "handleActionMirror");

        emulateLongWork(id);
        Bitmap sourceBitmap = BitmapFactory.decodeFile(filename);
        Bitmap bmOverlay = Bitmap.createBitmap(sourceBitmap.getWidth() / 2, sourceBitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bmOverlay);

        c.drawBitmap(sourceBitmap, 0, 0, null);
        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap.getWidth(), sourceBitmap.getHeight(), Bitmap.Config.ARGB_8888);

        c = new Canvas(resultBitmap);
        c.drawBitmap(bmOverlay, 0, 0, null);

        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);

        bmOverlay = Bitmap.createBitmap(bmOverlay, 0, 0, bmOverlay.getWidth(), bmOverlay.getHeight(), matrix, false);
        c.drawBitmap(bmOverlay, sourceBitmap.getWidth() / 2, 0, null);

        makeResult(resultBitmap, id);
    }

    private void handleActionInvert(String filename, int id) {
        Log.d(LOG_TAG, "handleActionInvert");
        emulateLongWork(id);
        Bitmap sourceBitmap = BitmapFactory.decodeFile(filename);
        int[] array = new int[sourceBitmap.getWidth() * sourceBitmap.getHeight()];
        sourceBitmap.getPixels(array, 0, sourceBitmap.getWidth(), 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight());

        for (int i = 0; i < array.length; i++) {
            int a = (array[i] & 0xff000000) >> 24;
            int r = (array[i] & 0xff0000) >> 16;
            int g = (array[i] & 0x00ff00) >> 8;
            int b = (array[i] & 0x0000ff);
            r = 255 - r;
            g = 255 - g;
            b = 255 - b;
            array[i] = (a << 24) + ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
        }

        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap.getWidth(), sourceBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        resultBitmap.setPixels(array, 0, resultBitmap.getWidth(), 0, 0, resultBitmap.getWidth(), resultBitmap.getHeight());

        makeResult(resultBitmap, id);
    }

    private void emulateLongWork(int id) {
        int time = random.nextInt(25000) + 5000;
        int d = time / 200;
        for (int i = 0; i < d; i++) {
            sendProgress(id, (int) (i * 1.0 / d * 100));
            sleep(200);
        }
    }


    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
