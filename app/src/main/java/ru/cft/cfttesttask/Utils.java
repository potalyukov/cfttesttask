package ru.cft.cfttesttask;

import android.graphics.Bitmap;

import java.io.FileOutputStream;
import java.io.IOException;

public class Utils {
    public static boolean saveBitmapToFile(Bitmap bmp, String filename) {
        FileOutputStream out = null;
        boolean result = true;
        try {
            out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
