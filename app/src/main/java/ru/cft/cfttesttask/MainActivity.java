package ru.cft.cfttesttask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private ImageView picPreviewImg;
    private String currentPreviewBitmapFilename;
    private Button rotateBtn, mirrorBtn, invertBtn;
    private ListView picsListView;
    private ProgressBar progressBar;

    private Map<Integer, Integer> progresses = new HashMap<>();

    private Bitmap bitmap;
    private Handler handler;

    private ImagesCursorAdapter cursorAdapter;
    private ImagesDatabaseHelper dbHelper;

    private String menuItemFilename;
    private int menuItemId;

    private DownloadFileFromURLLoader fileFromURLLoader;

    public static final int PICK_IMAGE_REQUEST_CODE = 1;
    public static final int REQUEST_IMAGE_CAPTURE = 2;
    public static final int DB_LOADER_ID = 0;
    public static final int FILE_DOWNLOAD_LOADER_ID = 1;

    public static final String PARAM_FILENAME = "ru.cft.cfttesttask.param.FILENAME";
    public static final String STATE_PROGRESSBAR = "ru.cft.cfttesttask.param.STATE_PROGRESSBAR";
    public static final String INSTANCE_STATE_FILENAME = "ru.cft.cfttesttask.state.FILENAME";
    public static final int MSG_SET_PROGRESS = 0;
    public static final int MSG_FINISHED = 1;

    public static final String LOG_TAG = MainActivity.class.getCanonicalName();

    private LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
            return new ImagesCursorLoader(MainActivity.this, dbHelper);
        }


        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            cursorAdapter.swapCursor(cursor);
            Log.d(LOG_TAG, "cursor load finished");
            picsListView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    private LoaderManager.LoaderCallbacks<String> downloadLoaderCallbacks = new LoaderManager.LoaderCallbacks<String>() {
        @Override
        public Loader<String> onCreateLoader(int i, Bundle bundle) {
            return new DownloadFileFromURLLoader(MainActivity.this);
        }

        @Override
        public void onLoadFinished(Loader<String> loader, String filename) {

        }

        @Override
        public void onLoaderReset(Loader<String> loader) {

        }
    };

    private Bitmap getLastBitmap() {
        String filename = getLastFilename();
        if (filename == null) {
            return null;
        }
        return BitmapFactory.decodeFile(filename);
    }

    private String getLastFilename() {
        List<ImagesDBItem> items = dbHelper.getAllImagesDBItems();
        for (ImagesDBItem item : items) {
            if (item.getFilename() != null) {
                return item.getFilename();
            }
        }
        return null;
    }

    private void setEnabledBtns(boolean enabled) {
        rotateBtn.setEnabled(enabled);
        mirrorBtn.setEnabled(enabled);
        invertBtn.setEnabled(enabled);
    }

    private void initViews(Bundle savedInstanceState) {
        rotateBtn = (Button) findViewById(R.id.btn_rotate);
        rotateBtn.setOnClickListener(this);

        mirrorBtn = (Button) findViewById(R.id.btn_mirror);
        mirrorBtn.setOnClickListener(this);

        invertBtn = (Button) findViewById(R.id.btn_invert);
        invertBtn.setOnClickListener(this);

        picPreviewImg = (ImageView) findViewById(R.id.pic_preview);
        picPreviewImg.setOnClickListener(this);

        if (savedInstanceState != null) {
            currentPreviewBitmapFilename = savedInstanceState.getString(INSTANCE_STATE_FILENAME);
        }

        if (currentPreviewBitmapFilename != null) {
            picPreviewImg.setImageBitmap(BitmapFactory.decodeFile(currentPreviewBitmapFilename));
        } else {
            Bitmap lastBitmap = getLastBitmap();
            if (lastBitmap != null) {
                picPreviewImg.setImageBitmap(lastBitmap);
                currentPreviewBitmapFilename = getLastFilename();
            } else {
                picPreviewImg.setImageResource(R.drawable.camera);
                currentPreviewBitmapFilename = null;
                setEnabledBtns(false);
            }
        }
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        if (savedInstanceState != null) {
            switch (savedInstanceState.getInt(STATE_PROGRESSBAR)) {
                case View.VISIBLE:
                    progressBar.setVisibility(View.VISIBLE);
                    break;
                case View.INVISIBLE:
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
            }
        }

        picsListView = (ListView) findViewById(R.id.pics_list_view);
        picsListView.setVisibility(View.INVISIBLE);
        registerForContextMenu(picsListView);
        picsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getAdapter().getItem(position);
                menuItemFilename = cursor.getString(cursor.getColumnIndexOrThrow("filename"));
                menuItemId = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
                openContextMenu(parent);
            }
        });
        picsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getAdapter().getItem(position);
                menuItemFilename = cursor.getString(cursor.getColumnIndexOrThrow("filename"));
                menuItemId = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
                openContextMenu(parent);
                return true;
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        switch (item.getItemId()) {
            case R.id.context_menu_use:
                Log.d(LOG_TAG, "use " + menuItemFilename);
                currentPreviewBitmapFilename = menuItemFilename;
                loadBitmapToImageView(menuItemFilename, picPreviewImg);
                break;
            case R.id.context_menu_delete:
                Log.d(LOG_TAG, "delete " + menuItemFilename);
                dbHelper.deleteImagesDBItem(menuItemFilename);
                getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
                break;
        }
        return true;
    }

    public void addProgress(int id, int progress) {
        progresses.put(id, progress);
    }

    public int getProgress(int id) {
        try {
            return progresses.get(id);
        } catch (Exception e) {
            return 0;
        }
    }

    public void loadImage(String url) {
        fileFromURLLoader = (DownloadFileFromURLLoader) getSupportLoaderManager().initLoader(FILE_DOWNLOAD_LOADER_ID, null, downloadLoaderCallbacks);
        fileFromURLLoader.setUrl(url);
        fileFromURLLoader.setHandler(handler);
        fileFromURLLoader.forceLoad();
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, PICK_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_PROGRESSBAR, progressBar.getVisibility());
        outState.putString(INSTANCE_STATE_FILENAME, currentPreviewBitmapFilename);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new ImagesDatabaseHelper(this);

        initViews(savedInstanceState);


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case MSG_FINISHED:
                        progressBar.setVisibility(View.INVISIBLE);
                        String filename = msg.getData().getString(PARAM_FILENAME);
                        dbHelper.addImagesDBItem(new ImagesDBItem(filename));
                        currentPreviewBitmapFilename = filename;
                        loadBitmapToImageView(filename, picPreviewImg);
                        setEnabledBtns(true);
                        getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
                        break;
                    case MSG_SET_PROGRESS:
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.setProgress(msg.arg1);
                        break;
                }
            }
        };

        FilterService.startActionCheckServiceState(this);

        fileFromURLLoader = (DownloadFileFromURLLoader) getSupportLoaderManager().getLoader(FILE_DOWNLOAD_LOADER_ID);

        cursorAdapter = new ImagesCursorAdapter(this, dbHelper.getImagesCursor());

        picsListView.setAdapter(cursorAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(filterServiceReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(FilterService.ACTION_RESULT_DONE);
        intentFilter.addAction(FilterService.ACTION_RESULT_PROGRESS);
        intentFilter.addAction(FilterService.ACTION_RESULT_SERVICE_STATE);
        registerReceiver(filterServiceReceiver, intentFilter);
        if (fileFromURLLoader != null) {
            fileFromURLLoader.setHandler(handler);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }

    @Override
    public void onClick(View v) {
        int id = 0;
        switch (v.getId()) {
            case R.id.btn_rotate:
                id = dbHelper.addImagesDBItem(new ImagesDBItem(null));
                getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
                FilterService.startActionRotate(this, currentPreviewBitmapFilename, id);
                break;
            case R.id.btn_mirror:
                id = dbHelper.addImagesDBItem(new ImagesDBItem(null));
                getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
                FilterService.startActionMirror(this, currentPreviewBitmapFilename, id);
                break;
            case R.id.btn_invert:
                id = dbHelper.addImagesDBItem(new ImagesDBItem(null));
                getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
                FilterService.startActionInvert(this, currentPreviewBitmapFilename, id);
                break;
            case R.id.pic_preview:
                DialogFragment dialog = new SelectDialog();
                dialog.show(getSupportFragmentManager(), "select_dialog");
                break;
        }
    }

    public static void makeErrorToast(Context context) {
        Toast toast = Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String filename = null;
        try {
            filename = getExternalFilesDir(null).getAbsolutePath() + "/" + System.currentTimeMillis() + ".png";
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (resultCode != Activity.RESULT_OK) {
            makeErrorToast(this);
        } else if (requestCode == PICK_IMAGE_REQUEST_CODE) {
            try {
                if (bitmap != null) {
                    bitmap.recycle();
                }
                InputStream stream = getContentResolver().openInputStream(
                        data.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                stream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
            Bundle extras = data.getExtras();
            if (bitmap != null) {
                bitmap.recycle();
            }
            bitmap = (Bitmap) extras.get("data");
        }
        if (filename != null && Utils.saveBitmapToFile(bitmap, filename)) {
            dbHelper.addImagesDBItem(new ImagesDBItem(filename));
            getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
            currentPreviewBitmapFilename = filename;
            picPreviewImg.setImageBitmap(bitmap);
            setEnabledBtns(true);
        } else {
            makeErrorToast(this);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private static class DownloadFileFromURLLoader<T> extends AsyncTaskLoader<String> {

        private String url;
        private Handler handler;

        public void setUrl(String url) {
            this.url = url;
        }

        public void setHandler(Handler handler) {
            this.handler = handler;
        }

        public Handler getHandler() {
            return handler;
        }

        public DownloadFileFromURLLoader(Context context) {
            super(context);
        }


        @Override
        public String loadInBackground() {
            int count;
            InputStream input = null;
            OutputStream output = null;
            String filename = null;
            try {
                URL url = new URL(this.url);
                URLConnection conection = url.openConnection();
                conection.connect();

                int lenghtOfFile = conection.getContentLength();

                input = new BufferedInputStream(url.openStream(), 8192);
                filename = getContext().getExternalFilesDir(null).getAbsolutePath() + "/downloaded/";
                File file = new File(filename);
                if (!file.exists()) {
                    file.mkdir();
                }
                filename += System.currentTimeMillis();
                output = new FileOutputStream(filename);


                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    int progress = (int) ((total * 100) / lenghtOfFile);
                    if (handler != null)
                        handler.sendMessage(createProgressMessage(progress));
                    Log.d(LOG_TAG, "downloading..." + progress);
                    output.write(data, 0, count);
                }

                output.flush();
                if (handler != null)
                    handler.sendMessage(createFinishedMessage(filename));

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            } finally {
                try {
                    output.close();
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return filename;
        }

        @Override
        protected void onForceLoad() {
            super.onForceLoad();
            handler.sendMessage(createProgressMessage(0));
        }
    }

    public void updateProgressBar(int progress) {
        progressBar.setProgress(progress);
    }

    public static Message createProgressMessage(int progress) {
        Message msg = new Message();
        msg.what = MSG_SET_PROGRESS;
        msg.arg1 = progress;
        return msg;
    }

    public static Message createFinishedMessage(String filename) {
        Message msg = new Message();
        msg.what = MSG_FINISHED;
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_FILENAME, filename);
        msg.setData(bundle);
        return msg;
    }

    public void loadBitmapToImageView(String filename, ImageView img) {
        Bitmap bm = BitmapFactory.decodeFile(filename);
        img.setImageBitmap(bm);
        setEnabledBtns(true);
    }

    private BroadcastReceiver filterServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int id = intent.getIntExtra(FilterService.EXTRA_ID, 0);
            String filename = intent.getStringExtra(FilterService.EXTRA_FILENAME);
            switch (intent.getAction()) {
                case FilterService.ACTION_RESULT_DONE:
                    currentPreviewBitmapFilename = getLastFilename();
                    loadBitmapToImageView(filename, picPreviewImg);
                    getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
                    break;
                case FilterService.ACTION_RESULT_PROGRESS:
                    int v = intent.getIntExtra(FilterService.EXTRA_PROGRESS, 0);
                    addProgress(id, v);
                    picsListView.invalidateViews();
                    break;
                case FilterService.ACTION_RESULT_SERVICE_STATE:
                    boolean serviceIsRunning = intent.getBooleanExtra(FilterService.EXTRA_IS_RUNNING, false);
                    if (!serviceIsRunning) {
                        dbHelper.cleanUp();

                        getSupportLoaderManager().initLoader(DB_LOADER_ID, null, cursorLoaderCallbacks);
                        getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
                    } else {
                        getSupportLoaderManager().initLoader(DB_LOADER_ID, null, cursorLoaderCallbacks);
                    }
                    break;
            }

        }
    };

    private static class ImagesCursorLoader extends CursorLoader {

        private ImagesDatabaseHelper db;

        public ImagesCursorLoader(Context context, ImagesDatabaseHelper db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getImagesCursor();
            return cursor;
        }

    }
}
