package ru.cft.cfttesttask;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ImagesDatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "imagesDatabase";

    private static final String TABLE_IMAGES = "images";

    private static final String KEY_ID = "_id";
    private static final String KEY_FILENAME = "filename";

    public ImagesDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_IMAGES_TABLE = "CREATE TABLE " + TABLE_IMAGES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_FILENAME + " TEXT)";
        db.execSQL(CREATE_IMAGES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);
            onCreate(db);
        }
    }

    public Cursor getImagesCursor() {
        SQLiteDatabase db = getWritableDatabase();
        return db.rawQuery("SELECT  * FROM " + TABLE_IMAGES + " ORDER BY " + KEY_ID + " DESC", null);
    }

    public int addImagesDBItem(ImagesDBItem item) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FILENAME, item.getFilename());
        int id = (int) db.insertOrThrow(TABLE_IMAGES, null, values);
        db.close();
        return id;
    }

    public List<ImagesDBItem> getAllImagesDBItems() {
        List<ImagesDBItem> imagesDBItems = new ArrayList<ImagesDBItem>();
        String selectQuery = "SELECT  * FROM " + TABLE_IMAGES + " ORDER BY " + KEY_ID + " DESC";

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ImagesDBItem item = new ImagesDBItem(cursor.getString(1));
                item.setId(cursor.getInt(0));
                imagesDBItems.add(item);
            } while (cursor.moveToNext());
        }

        if (cursor != null)
            cursor.close();

        return imagesDBItems;
    }

    public int updateImagesDBItem(ImagesDBItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FILENAME, item.getFilename());
        int result = db.update(TABLE_IMAGES, values, KEY_ID + " = ?",
                new String[]{String.valueOf(item.getId())});
        db.close();
        return result;
    }

    public void deleteImagesDBItem(ImagesDBItem item) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_IMAGES, KEY_ID + " = ?",
                new String[]{String.valueOf(item.getId())});
        db.close();
    }

    public void deleteImagesDBItem(String filename) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_IMAGES, KEY_FILENAME + " = ?", new String[]{filename});
        db.close();
    }

    public void cleanUp() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_IMAGES, KEY_FILENAME + " IS NULL", null);
        db.close();
    }

}