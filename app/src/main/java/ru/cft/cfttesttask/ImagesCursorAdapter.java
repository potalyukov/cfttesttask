package ru.cft.cfttesttask;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v4.widget.CursorAdapter;

public class ImagesCursorAdapter extends CursorAdapter {

    private MainActivity mainActivity;

    public ImagesCursorAdapter(MainActivity context, Cursor cursor) {
        super(context, cursor, 0);
        mainActivity = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView imageView = (ImageView) view.findViewById(R.id.list_item_img);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.item_progress_bar);
        String filename = cursor.getString(cursor.getColumnIndexOrThrow("filename"));
        int id = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        if (id % 2 == 0) {
            view.setBackgroundColor(context.getResources().getColor(R.color.list_color_1));
        } else {
            view.setBackgroundColor(context.getResources().getColor(R.color.list_color_2));
        }
        if (filename == null) {
            progressBar.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            progressBar.setProgress(mainActivity.getProgress(id));
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            imageView.setVisibility(View.VISIBLE);
            Bitmap bmImg = BitmapFactory.decodeFile(filename);
            imageView.setImageBitmap(bmImg);
        }
    }
}
